package shop;

import shop.menu.MenuAdmin;
import shop.order.Shop;

import java.util.Scanner;

public class Administrator extends User {

    public Administrator(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void loadMenu(Shop shop) {
        Scanner in = new Scanner(System.in);
        MenuAdmin menu = new MenuAdmin(shop);
        menu.main();

    }

    public void exit(){

    }
}