package shop;

import shop.order.Shop;
import shop.product.*;

import java.util.Scanner;


public class Main {
    private final String ADMIN = "ADMINISTRATOR";
    private User user;

    private Shop shop;

    public static void main(String[] args) {

        Main host = new Main();
        host.shop = new Shop();
        host.shop.addProduct(new Product(2, 600, "SonyEarbuds"));
        host.shop.addProduct(new Product(3, 2200, "JavaBook"));
        host.shop.addProduct(new Product(4, 1300, "iPhone"));
        host.shop.addProduct(new Product(5, 4000, "Watch3"));
        host.shop.addProduct(new Product(3, 5000, "Apple"));
        host.shop.addProduct(new Product(4, 10000, "Oculus"));
        host.shop.addProduct(new Product(4, 2100, "Вентилятор"));
        host.shop.addProduct(new SmartPhone(10, 10000, "Samsung Galaxy A32", 10));
        host.shop.addProduct(new GamePC(5, 100000, "Игровой ПК Invasion Pandora", false));
        host.shop.addProduct(new GameConsole(10, 30000,"PlayStation 5 Digital Edition", 9, 2));
        boolean isRunning = true;
        Scanner in = new Scanner(System.in);
        while (isRunning) {

            System.out.println("1: Ввести имя");
            System.out.println("0: Выход");
            int choice = 0;
            choice = in.nextInt();

            switch (choice) {
                case 1:
                    String name = in.next();
                    if (name.equalsIgnoreCase(host.ADMIN)) {
                        host.user = new Administrator(name);

                    } else {
                        host.user = new Buyer(name);
                    }
                    host.user.loadMenu(host.shop);
                    break;
                case 0:
                    System.out.println("До свидания!");
                    return;
            }
        }
    }
}