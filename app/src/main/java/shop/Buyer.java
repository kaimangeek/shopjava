package shop;

import shop.menu.MenuBuyer;
import shop.order.Order;
import shop.order.Shop;

import java.util.Scanner;

public class Buyer extends User {


    public Buyer(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void loadMenu(Shop shop) {
        Order order = new Order(this, shop.currentId);
        shop.currentId++;
        Scanner in = new Scanner(System.in);
        MenuBuyer menu = new MenuBuyer(shop, order);
        menu.main();

    }
}