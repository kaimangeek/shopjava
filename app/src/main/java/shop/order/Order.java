package shop.order;

import shop.User;
import shop.order.OrderItem;

import java.util.ArrayList;
import java.util.List;

public class Order {
    private List<OrderItem> orders;
    private String status;
    private User user;
    private int id;

    public List<OrderItem> getOrders() {
        return orders;
    }

    Order(User user, int id) {
        orders = new ArrayList<>();
        status = "Не подтверждён";
        this.user = user;
        this.id = id;
    }

    public void editingOrder(OrderItem orderItem) {
        if (orderItem.getProduct() == null) {
            return;
        }
        for (OrderItem item : orders) {
            if (item.getProduct().getTitle().equalsIgnoreCase(orderItem.getProduct().getTitle())) {
                return;
            }
        }
        if (orderItem.getCount() > orderItem.getProduct().getCount()) {
            orderItem.setCount(orderItem.getProduct().getCount());
        }
        orderItem.getProduct().setCount(orderItem.getProduct().getCount() - orderItem.getCount());
        orders.add(orderItem);
    }

    public void editCount(String prod, int count) {
        if (count <= 0) {return;}
        for (OrderItem item : orders) {
            if (item.getProduct().getTitle().equalsIgnoreCase(prod)) {
                if (count < item.getCount()) {
                    item.getProduct().setCount(item.getProduct().getCount() + (item.getCount() - count));
                    item.setCount(count);
                    return;
                }
                if ((item.getProduct().getCount() + item.getCount()) >= count) {
                    item.getProduct().setCount(item.getProduct().getCount() + item.getCount());
                    item.setCount(count);
                    item.getProduct().setCount(item.getProduct().getCount() - item.getCount());
                    break;
                } else {
                    item.setCount(item.getCount() + item.getProduct().getCount());
                    item.getProduct().setCount(0);
                }
            }
        }
    }

    public int priceSum() {
        int price = 0;
        for (OrderItem item : orders) {
            price += item.getProduct().getPrice() * item.getCount();
        }
        return price;
    }

    public void deleteOrderItem(String productName) {
        for (OrderItem item : orders) {
            if (item.getProduct().getTitle().equalsIgnoreCase(productName)) {
                item.getProduct().setCount(item.getProduct().getCount() + item.getCount());
                orders.remove(item);
                break;
            }
        }
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Заказ{" +
                "сумма=" + priceSum() +
                ", товары=" + orders + '\'' +
                ", статус='" + status + '\'' +
                ", пользователь=" + user.getName() +
                ", идентификатор=" + id +
                '}';
    }
}