package shop.order;

import shop.product.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Shop {
    private List<Order> orders;
    private List<Product> catalog;
    public int currentId = 0;

    public void addOrder(Order order) {
        this.orders.add(order);
    }

    public void addProduct(Product product) {
        catalog.add(product);
    }

    public void printCatalog() {
        for (Product product : catalog) {
            System.out.println(product);
        }
    }

    public Product getProduct(String title) {
        for (Product product : catalog) {
            if (product.getTitle().equalsIgnoreCase(title)) {
                return product;
            }
        }
        return null;
    }

    public void printOrders() {
        for (Order order : orders) {
            System.out.println(order);
        }
    }

    public void removeOrder(int id) {
        for (Order order : orders) {
            if (order.getId() == id) {
                orders.remove(order);
                break;
            }
        }
    }

    public void workOrder(int id) {
        for (Order order : orders) {
            if (order.getId() == id) {
                order.setStatus("В работе");
                break;
            }
        }
    }

    public void editProduct(String name) {
        for (Product product : catalog) {
            if (product.getTitle().equalsIgnoreCase(name)) {
                product.edit(new Scanner(System.in));
                break;
            }
        }
    }


    public Shop() {
        orders = new ArrayList<>();
        catalog = new ArrayList<>();
    }
}