package shop;

import shop.order.Shop;

public abstract class User {
    protected String name;

    public abstract String getName();

    public abstract void loadMenu(Shop shop);
}