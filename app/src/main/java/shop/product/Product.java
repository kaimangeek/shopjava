package shop.product;

import java.util.Scanner;

public class Product {
    private int count;
    private int price;
    private String title;

    public Product() {}

    public Product(int count, int price, String title) {
        this.count = count;
        this.price = price;
        this.title = title;
    }

    public void edit(Scanner in) {
        System.out.println("Количество - " + count + ", новое:");
        count = in.nextInt();
        System.out.println("Цена - " + price + ", новая:");
        price = in.nextInt();
        System.out.println("Название - " + title + ", новое:");
        title = in.next();
    }

    public void input(Scanner in) {
        System.out.println("Количество - :");
        count = in.nextInt();
        System.out.println("Цена - ");
        price = in.nextInt();
        System.out.println("Название - ");
        title = in.next();
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return getTitle() + " " +getPrice() + "р, наличие: " + getCount();
    }
}