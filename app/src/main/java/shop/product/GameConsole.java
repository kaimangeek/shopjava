package shop.product;

public class GameConsole extends Product {
    private final int generation;
    private final int numberGamepads;


    public GameConsole(int count, int price, String title, int generation, int numberGamepads) {
        super(count, price, title);
        this.generation = generation;
        this.numberGamepads = numberGamepads;
    }

    public int getGeneration() {
        return this.generation;
    }

    public int getNumberGamepads() {
        return this.numberGamepads;
    }

    @Override
    public String toString() {
        return String.format("%s %d р, наличие: %d, поколение: %d, количество геймпадов: %d",
                this.getTitle(), this.getPrice(), this.getCount(), this.getGeneration(), this.getNumberGamepads());
    }

}