package shop.product;

public class SmartPhone extends Product {
    private final double displaySize;

    public SmartPhone(int count, int price, String title, int displaySize) {
        super(count, price, title);
        this.displaySize = displaySize;
    }

    public double getDisplaySize(){
        return this.displaySize;
    }

    @Override
    public String toString() {
        return String.format("%s %d р, наличие: %d, размер дисплея: %f",
                this.getTitle(), this.getPrice(),this.getCount(), this.getDisplaySize() );
    }

}