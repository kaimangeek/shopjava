package shop.product;

public class GamePC extends Product {
    private final boolean protativity;

    public GamePC(int count, int price, String title, boolean protativity) {
        super(count, price, title);
        this.protativity = protativity;
    }

    public boolean getProtativity() {
        return this.protativity;
    }

    @Override
    public String toString() {
        return String.format("%s %d р, наличие: %d, портативность: %b",
                this.getTitle(), this.getPrice(), this.getCount(), this.getProtativity());
    }

}