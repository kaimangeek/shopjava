package shop.menu;

import shop.order.Order;
import shop.order.OrderItem;

public class ActionExitBuyer implements Action {
    private final Order order;
    private final Menu menu;

    ActionExitBuyer(Order order, Menu menu) {
        this.order = order;
        this.menu = menu;
    }

    public void action() {
        for (OrderItem ord : this.order.getOrders()) {
            ord.getProduct().setCount(ord.getProduct().getCount() + ord.getCount());
        }
        this.menu.setIsRunning(false);
    }
}
