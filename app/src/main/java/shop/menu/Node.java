package shop.menu;

public class Node {
    private Node[] childrens;
    private Node parent;
    private boolean hasParent;
    private final String name;
    private final Action action;

    public Node(int countOfChildrens, Action action, String name) {
        this.childrens = new Node[countOfChildrens];
        this.hasParent = false;
        this.name = name;
        this.action = action;
    }

    public void setParent(Node parent) {
        this.parent = parent;
        this.hasParent = true;
    }

    public Node getParent() {
        return this.parent;
    }

    public void setChildrens(Node[] childrens) {
        this.childrens = childrens;
    }

    public Node[] getChildrens() {
        return this.childrens;
    }

    public boolean hasParent() {
        return this.hasParent;
    }

    public Node goToParent() {
        return this.parent;
    }

    public Node goToChild(int index) {
        return this.childrens[index];
    }

    public String getName() {
        return this.name;
    }

    public void doAction() {
        this.action.action();
    }

}