package shop.menu;

import shop.order.Order;
import shop.order.OrderItem;
import shop.order.Shop;

import java.util.Scanner;

public class ActionAddOrderItem implements Action {
    private final Order order;
    private final Shop shop;

    ActionAddOrderItem(Order order, Shop shop) {
        this.order = order;
        this.shop = shop;
    }

    public void action() {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите название товара и количество:");
        this.order.editingOrder(new OrderItem(this.shop.getProduct(in.next()), in.nextInt()));
    }
}