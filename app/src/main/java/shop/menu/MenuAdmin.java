package shop.menu;

import shop.order.Shop;

import java.util.Objects;
import java.util.Scanner;

import static shop.menu.Navigation.menuRun;

public class MenuAdmin implements Menu {
    private final Shop shop;
    private boolean isRunning;

    public MenuAdmin(Shop shop) {
        this.shop = shop;
    }

    public void setIsRunning(boolean check) {
        this.isRunning = check;
    }

    public void main() {
        Node menu = new Node(0, new ActionPrintCatalog(this.shop), "Меню");
        Node viewCatalog = new Node(0, new ActionPrintCatalog(this.shop), "Посмотреть каталог");
        Node addProduct = new Node(0, new ActionAddProduct(this.shop), "Добавить товар");
        Node editProduct = new Node(0, new ActionEditProduct(this.shop), "Отредактировать товар");
        Node seeOrders = new Node(0, new ActionPrintOrders(this.shop), "Посмотреть заказы");
        Node sendOrder = new Node(0, new ActionWorkOrder(this.shop), "Отправить заказ в работу");
        Node removeOrder = new Node(0, new ActionRemoveOrder(this.shop), "Удалить заказ");
        Node exit = new Node(0, new ActionExitAdmin(this), "Выход");
        Node[] nodes = {menu, viewCatalog, addProduct, editProduct, seeOrders, sendOrder, removeOrder, exit};
        Node[] childrens = {viewCatalog, addProduct, editProduct, seeOrders, sendOrder, removeOrder, exit};
        menu.setChildrens(childrens);
        viewCatalog.setParent(menu);
        addProduct.setParent(menu);
        editProduct.setParent(menu);
        seeOrders.setParent(menu);
        sendOrder.setParent(menu);
        removeOrder.setParent(menu);
        exit.setParent(menu);

        menuRun(menu, childrens, this.isRunning);

    }
}
