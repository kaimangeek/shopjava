package shop.menu;

import shop.order.Shop;

public class ActionPrintOrders implements Action {
    private final Shop shop;

    ActionPrintOrders(Shop shop) {
        this.shop = shop;
    }

    public void action() {
        this.shop.printOrders();
    }
}