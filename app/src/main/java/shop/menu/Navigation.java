package shop.menu;

import java.util.Objects;
import java.util.Scanner;

public class Navigation{

    public static void menuRun(Node menu, Node[] childrens, boolean isRunning) {

        isRunning = true;
        Node node = menu;

        char emptyActionChar = '-';
        String a = "";
        while (isRunning) {
            if (node.getName().equals("Меню")) {
                System.out.format("%s\n", node.getName());
                childrens = node.getChildrens();
                for (int i = 0; i < childrens.length; i++) {
                    System.out.format("    %d %s\n", i + 1, childrens[i].getName());
                }
            } else {
                System.out.format("1 %s\n", node.getName());
                childrens = node.getChildrens();
                for (int i = 0; i < childrens.length; i++) {
                    System.out.format("    %d %s\n", i + 2, childrens[i].getName());
                }
            }

            System.out.println("Введите номер пункта меню для его выбора");
            System.out.println("Или введите 0 чтобы перейти в вышестоящий каталог");
            Scanner sc = new Scanner(System.in);
            a = sc.nextLine();
            if (Objects.equals(a, "0")) {
                if (node.hasParent()) {
                    node = node.goToParent();
                    a = "";
                } else {
                    System.out.println("Нет вышестоящего каталога, попробуйте другую команду");
                    a = "";
                }
            } else {
                try {
                    int numberOfChild = Integer.parseInt(a);
                    if (1 <= numberOfChild && numberOfChild <= node.getChildrens().length) {
                        if (node.getChildrens().length > 0) {
                            node = node.goToChild(numberOfChild - 1);
                        }
                        node.doAction();
                    } else {
                        System.out.println("Нет подпункта меню с выбранным номером");
                    }
                } catch (NumberFormatException | NullPointerException e) {
                    System.out.println("Введите число");

                }
            }

        }
    }
}

