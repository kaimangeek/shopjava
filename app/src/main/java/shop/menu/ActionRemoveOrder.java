package shop.menu;

import shop.order.Shop;

import java.util.Scanner;

public class ActionRemoveOrder implements Action {
    private final Shop shop;

    ActionRemoveOrder(Shop shop) {
        this.shop = shop;

    }

    public void action() {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите id заказа");
        this.shop.removeOrder(in.nextInt());
        System.out.println("Заказ удалён");
    }
}