package shop.menu;

import shop.order.Order;
import shop.order.Shop;

import java.util.Objects;
import java.util.Scanner;

import static shop.menu.Navigation.menuRun;

public class MenuBuyer implements Menu {
    private final Shop shop;
    private boolean isRunning;
    private final Order order;

    public MenuBuyer(Shop shop, Order order) {
        this.shop = shop;
        this.order = order;
    }

    public void setIsRunning(boolean check) {
        this.isRunning = check;
    }

    public void main() {
        Node menu = new Node(0, new ActionPrintCatalog(this.shop), "Меню");
        Node viewCatalog = new Node(0, new ActionPrintCatalog(this.shop), "Посмотреть каталог");
        Node viewBasket = new Node(0, new ActionPrintOrder(this.order), "Посмотреть корзину");
        Node addOrderItem = new Node(0, new ActionAddOrderItem(this.order, this.shop), "Добавить товар в корзину");
        Node removeProduct = new Node(0, new ActionDeleteOrderPosition(this.order), "Удалить товар из корзины");
        //Node editProduct = new Node(0, new ActionEditOrder(this.order), "Редактировать товар в корзине");
        Node doOrder = new Node(0, new ActionAddOrder(this.order, this.shop, this), "Сделать заказ");
        Node exit = new Node(0, new ActionExitBuyer(this.order, this), "Выйти (заказ без подтверждения будет отменен)");
        Node[] nodes = {menu, viewCatalog, viewBasket, addOrderItem, removeProduct, doOrder, exit};
        Node[] childrens = {viewCatalog, viewBasket, addOrderItem, removeProduct, doOrder, exit};
        menu.setChildrens(childrens);
        viewCatalog.setParent(menu);
        viewBasket.setParent(menu);
        addOrderItem.setParent(menu);
        removeProduct.setParent(menu);
        //editProduct.setParent(menu);
        doOrder.setParent(menu);
        exit.setParent(menu);

        menuRun(menu, childrens, this.isRunning);

    }
}
