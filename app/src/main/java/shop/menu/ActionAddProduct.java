package shop.menu;

import shop.product.Product;
import shop.order.Shop;

import java.util.Scanner;

public class ActionAddProduct implements Action {
    private final Shop shop;

    ActionAddProduct(Shop shop) {
        this.shop = shop;
    }

    public void action() {
        Scanner in = new Scanner(System.in);
        Product product = new Product();
        product.input(in);
        this.shop.addProduct(product);
    }
}