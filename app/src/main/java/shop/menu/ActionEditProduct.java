package shop.menu;

import shop.order.Shop;

import java.util.Scanner;

public class ActionEditProduct implements Action {
    private final Shop shop;

    ActionEditProduct(Shop shop) {
        this.shop = shop;
    }

    public void action() {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите название продукта");
        String prodName = in.next();
        this.shop.editProduct(prodName);
    }
}