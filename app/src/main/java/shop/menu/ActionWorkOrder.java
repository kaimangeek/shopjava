package shop.menu;

import shop.order.Shop;

import java.util.Scanner;

public class ActionWorkOrder implements Action {
    private final Shop shop;

    ActionWorkOrder(Shop shop) {
        this.shop = shop;
    }

    public void action() {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите id заказа");
        this.shop.workOrder(in.nextInt());
        System.out.println("Начата работа по заказу");
    }
}