package shop.menu;

public class ActionExitAdmin implements Action {

    private final Menu menu;

    ActionExitAdmin(Menu menu) {

        this.menu = menu;
    }

    public void action() {
        this.menu.setIsRunning(false);
    }
}