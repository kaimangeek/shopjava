package shop.menu;

import shop.order.Shop;

public class ActionPrintCatalog implements Action {
    private final Shop shop;

    ActionPrintCatalog(Shop shop) {
        this.shop = shop;
    }

    public void action() {
        this.shop.printCatalog();
    }
}