package shop.menu;

import shop.order.Order;
import shop.order.Shop;

public class ActionAddOrder implements Action {
    private final Order order;
    private final Shop shop;
    private final Menu menu;

    ActionAddOrder(Order order, Shop shop, Menu menu) {
        this.order = order;
        this.shop = shop;
        this.menu = menu;
    }

    public void action() {
        this.shop.addOrder(order);
        menu.setIsRunning(false);
    }
}