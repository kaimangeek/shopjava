package shop.menu;

import shop.order.Order;

public class ActionPrintOrder implements Action {
    private final Order order;

    ActionPrintOrder(Order order) {
        this.order = order;
    }

    public void action() {
        System.out.println(this.order);
    }
}