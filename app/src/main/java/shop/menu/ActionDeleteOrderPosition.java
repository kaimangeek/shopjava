package shop.menu;

import shop.order.Order;

import java.util.Scanner;

public class ActionDeleteOrderPosition implements Action {
    private final Order order;

    ActionDeleteOrderPosition(Order order) {
        this.order = order;
    }

    public void action() {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите название товара");
        this.order.deleteOrderItem(in.next());
    }
}