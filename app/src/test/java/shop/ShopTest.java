package shop;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

class ShopTest {

    @Test
    void addOrder() {
        Shop shop = new Shop();
        shop.addOrder(new Order(new Buyer("Echo"), 1));
    }

    @Test
    void addProduct() {
        Shop shop = new Shop();
        shop.addProduct(new Product(10, 15, "Milk"));
        assertEquals("Milk", shop.getProduct("Milk").getTitle());
    }

    @Test
    void printCatalog() {
        Shop shop = new Shop();
        shop.addOrder(new Order(new Buyer("Echo"), 1));
        shop.printCatalog();
    }

    @Test
    void getProduct() {
        Shop shop = new Shop();
        shop.addProduct(new Product(10, 15, "Milk"));
        assertEquals("Milk", shop.getProduct("Milk").getTitle());
        assertNull(shop.getProduct("asdasd"));
    }

    @Test
    void printOrders() {
        Shop shop = new Shop();
        shop.addProduct(new Product(10, 15, "Milk"));
        Order o = new Order(new Buyer("Echo"), 1);
        shop.addOrder(o);
        shop.printOrders();
    }

    @Test
    void removeOrder() {
        Shop shop = new Shop();
        shop.addProduct(new Product(10, 15, "Milk"));
        Order o = new Order(new Buyer("Echo"), 1);
        shop.addOrder(o);
        shop.removeOrder(1);
    }

    @Test
    void workOrder() {
        Shop shop = new Shop();
        Order o = new Order(new Buyer("Echo"), 1);
        shop.addOrder(o);
        shop.workOrder(1);
        assertEquals("В работе", o.getStatus());
    }

    @Test
    void editProduct() {
        String input = "5 10 by";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        Shop shop = new Shop();
        shop.addProduct(new Product(10, 15, "Milk"));
        shop.editProduct("Milk");
        assertEquals("by", shop.getProduct("by").getTitle());
    }

    @Test
    void removeProduct() {
    }
}