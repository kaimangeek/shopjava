package shop;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {

    @Test
    void edit() {
        String input = "5 10 by";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        Product product = new Product();
        product.edit(new Scanner(in));

        assertEquals("by", product.getTitle());
        assertEquals(5, product.getCount());
        assertEquals(10, product.getPrice());
    }

    @Test
    void input() {
        String input = "5 10 by";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        Product product = new Product();
        product.input(new Scanner(in));
        assertEquals(5, product.getCount());
        assertEquals(10, product.getPrice());
        assertEquals("by", product.getTitle());
    }
}