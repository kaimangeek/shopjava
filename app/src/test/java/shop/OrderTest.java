package shop;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    Order order = new Order(new Buyer("asd"), 10);

    @Test
    void getOrders() {
        assertEquals(0, order.getOrders().size());
    }

    @Test
    void editingOrder() {
        order.getOrders().clear();
        order.editingOrder(new OrderItem(new Product(10, 10, "Milk"), 9));

        order.editingOrder(new OrderItem(new Product(10, 10, "Milk"), 5));
        assertEquals(1, order.getOrders().size());
    }

    @Test
    void editCount() {
        Order order = new Order(new Buyer("asd"), 2);
        order.getOrders().clear();
        Product p = new Product(8, 10, "Milk");
        order.editingOrder(new OrderItem(p, 22));
        order.editingOrder(new OrderItem(null, 22));

        order.editCount("Milk", 20);
        order.editCount("Milk", 5);
        System.out.println(order);
        order.editCount("Milk", 6);
        System.out.println(order);
        System.out.println(order.getOrders().size());
        System.out.println(order.getOrders().get(0).getCount());
        assertEquals(6, order.getOrders().get(0).getCount());
    }

    @Test
    void priceSum() {
        Order order = new Order(new Buyer("asd"), 2);
        order.getOrders().clear();
        Product p = new Product(8, 10, "Milk");
        order.editingOrder(new OrderItem(p, 22));
        assertEquals(80, order.priceSum());
    }

    @Test
    void deleteOrderItem() {
        Order order = new Order(new Buyer("asd"), 2);
        order.getOrders().clear();
        Product p = new Product(8, 10, "Milk");
        order.editingOrder(new OrderItem(p, 22));

        Product p2 = new Product(8, 10, "Lilk");
        order.editingOrder(new OrderItem(p2, 22));
        order.deleteOrderItem("Lilk");
        assertEquals(1, order.getOrders().size());
    }

    @Test
    void setStatus() {

        Order order = new Order(new Buyer("asd"), 2);
        order.setStatus("Refund");
        assertEquals("Refund", order.getStatus());
    }

    @Test
    void getId() {
        Order order = new Order(new Buyer("asd"), 3);
        assertEquals(3, order.getId());
    }

    @Test
    void testToString() {
    }
}